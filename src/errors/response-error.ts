// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type GenericError } from '@ory/kratos-client'

export class CustomError implements GenericError {
  code?: number
  debug?: string
  details?: object
  id?: string
  message: string
  reason?: string
  request?: string
  status?: string

  constructor(props: GenericError) {
    this.code = props.code
    this.debug = props.debug
    this.details = props.details
    this.id = props.id
    this.message = props.message
    this.reason = props.reason
    this.request = props.request
    this.status = props.status

    Error.captureStackTrace(this, this.constructor)
  }
}
