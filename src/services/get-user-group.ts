// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  type RelationType,
  userTypeLabels,
  organizationModels,
  availableRoleModels,
} from '../utils/permission-models'
import { fetchKratosService } from '../utils/fetch-ory-service'
import { getPermissions } from './get-permissions'
import { getUserType } from '../utils/get-user-type'
import type authModelProps from '../authorization-model.json'

async function fetchThroughMultiplePages() {
  const query = new URLSearchParams()
  query.set('page_size', '500')
  const result = await fetchKratosService(
    `/admin/identities?${query.toString()}`,
  )
  return result
}

// NOTE This logic follows the following steps:
// 1. All subject_ids from KETO are fetched for the queried group
// 2. alle users are fetched from KRATOS. This API has no bulk fetch for id's, unless fetching them all
// 3. Users are filtered and aggregated based on their shared id
// 4. The remaining users their permissions are fetched

export const getUserGroup = async (
  object = null,
  relation: RelationType | null = null,
  namespace = null,
  authorizationModel: typeof authModelProps,
) => {

  // NOTE Get all existing users, kratos doesn't support bulk search queries as of yet
  const allIdentities = await fetchThroughMultiplePages().catch((e) => {
    console.log('issue fetching group:', e)
  })

  if (!allIdentities?.length) {
    throw 'Get usergroup identities issue'
  }

  const query = new URLSearchParams()
  if (namespace) query.set('namespace', namespace)
  if (object) query.set('object', object)
  if (relation) query.set('relation', relation)

  const queriedPermissions = await getPermissions(query.toString())

  // NOTE Filter by finding permission matches
  const filteredIdentities = allIdentities.filter((identity) =>
    queriedPermissions.some(
      (permission) => identity.id === permission.subject_id,
    ),
  )

  // NOTE Get all permissions from fetched users
  const identitiesIncludingPermissions = await Promise.all(
    filteredIdentities.map(async (identity) => {
      const query = new URLSearchParams({
        subject_id: identity.id,
      }).toString()

      const permissions = await getPermissions(query)

      const permissionsWithLabels = permissions.map((permission) => ({
        ...permission,
        ...organizationModels(authorizationModel).find(
          (model) => permission.object === model.id,
        ),
        ...availableRoleModels(authorizationModel).find(
          (model) => permission.object === model.id,
        ),
      }))

      const userType = getUserType(permissions, authorizationModel)

      const userTypeLabel = userTypeLabels.find(
        (label) => label.id === userType,
      )

      return {
        ...identity,
        permissions: permissionsWithLabels,
        userType: userTypeLabel,
      }
    }),
  )

  return identitiesIncludingPermissions
}
