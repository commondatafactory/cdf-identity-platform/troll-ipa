// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { fetchOryService } from '../utils/fetch-ory-service'

export const deleteUser = async ({
  userId,
}: {
  userId: string
}): Promise<boolean> =>
  await fetchOryService(
    `${process.env.KRATOS_ADMIN_URL}/admin/identities/${userId}`,
    {
      method: 'DELETE',
    },
  )
