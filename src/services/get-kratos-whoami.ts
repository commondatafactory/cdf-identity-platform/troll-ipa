// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { CustomError } from '../errors/response-error'
import { fetchOryService } from '../utils/fetch-ory-service'
import { type SessionWithIdentity } from '../types/types'

export const getKratosWhoami = async (
  cookie: string | undefined,
): Promise<SessionWithIdentity> => {
  if (!cookie) {
    throw new CustomError({
      message: 'Forbidden',
      debug: 'No cookie found',
      code: 401,
    })
  }

  const session = await fetchOryService(
    `${process.env.KRATOS_PUBLIC_URL}/sessions/whoami`,
    {
      method: 'GET',
      headers: { Cookie: cookie },
    },
  )

  if (!session.identity) {
    throw new CustomError({
      message: 'No identity found on session',
      code: 500,
    })
  }

  return session
}
