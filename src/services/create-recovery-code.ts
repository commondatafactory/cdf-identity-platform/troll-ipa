// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type RecoveryCodeForIdentity } from '@ory/kratos-client'

import { fetchOryService } from '../utils/fetch-ory-service'

export const createRecoveryCode = async ({
  id,
}: {
  id: string
}): Promise<RecoveryCodeForIdentity> =>
  await fetchOryService(`${process.env.KRATOS_ADMIN_URL}/admin/recovery/code`, {
    body: JSON.stringify({
      identity_id: id,
      expires_in: '0ns0us0ms0s0m168h', // 7 days
    }),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  })
