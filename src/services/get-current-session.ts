// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { getKratosWhoami } from './get-kratos-whoami'
import { getPermissions } from './get-permissions'
import { getUserType } from '../utils/get-user-type'
import { type UserType } from '../utils/permission-models'
import type authModelProps from '../authorization-model.json'

export const currentUser = async (
  cookie: string,
  authorizationModel: typeof authModelProps,
) => {
  const loggedInUserSession = await getKratosWhoami(cookie)

  const query = new URLSearchParams({
    subject_id: loggedInUserSession.identity.id,
  }).toString()

  const loggedInUserPermissions = await getPermissions(query)

  const loggedInUserType: UserType = getUserType(
    loggedInUserPermissions,
    authorizationModel,
  )

  return { loggedInUserSession, loggedInUserType, loggedInUserPermissions }
}
