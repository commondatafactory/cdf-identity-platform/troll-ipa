// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type CustomRelationTuple } from '../utils/permission-models'
import { fetchOryService } from '../utils/fetch-ory-service'
import { type SessionWithIdentity } from '../types/types'

const deleteTupleRequest = async (tuple: any) => {
  const tupleParameters = new URLSearchParams(tuple).toString()

  return await fetchOryService(
    `${process.env.KETO_WRITE_URL}/admin/relation-tuples?${tupleParameters}`,
    {
      method: 'DELETE',
    },
  )
}

// TODO: only use user id as parameter, not the full user
export const deletePermissions = async (
  session: SessionWithIdentity | string,
  tuples: Array<Partial<CustomRelationTuple>> | Partial<CustomRelationTuple>,
) => {
  if (!Array.isArray(tuples)) {
    return await deleteTupleRequest(tuples)
  }

  const deletedPermissions = await Promise.all(
    tuples.map(async (permission) => {
      const tuple = {
        namespace: permission.namespace || null,
        object: permission.object || null,
        relation: permission.relation || null,
        [permission.subject_set ? 'subject_set' : 'subject_id']:
          permission.subject_set
            ? permission.subject_set
            : typeof session === 'string'
              ? session
              : session?.identity.id || null,
      }

      return await deleteTupleRequest(tuple)
    }),
  )

  return deletedPermissions
}
