// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Identity } from '@ory/kratos-client'

import { fetchOryService } from '../utils/fetch-ory-service'

export const getUserByField = async ({
  email,
}: {
  email: string
}): Promise<Identity | null> => {
  const users = await fetchOryService(
    `${process.env.KRATOS_ADMIN_URL}/admin/identities?credentials_identifier=${email}`,
  )

  return users[0] || null
}
