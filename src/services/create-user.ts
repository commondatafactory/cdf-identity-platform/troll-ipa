// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Identity } from '@ory/kratos-client'
import crypto from 'node:crypto'

import { fetchOryService } from '../utils/fetch-ory-service'

export const createUser = async ({
  traits,
  password,
  metadata_public = null,
  identityData,
  verified,
}: {
  traits?: Identity['traits']
  password?: string
  metadata_public?: any
  identityData?: Identity
  verified?: boolean
}): Promise<Identity> => {
  const identity = identityData ?? {
    metadata_public,
    traits,
    verifiable_addresses: [
      {
        status: verified ? 'completed' : 'pending',
        verified,
        id: crypto.randomUUID(),
        value: traits.email,
        via: 'email',
      },
    ],
    credentials: {
      password: {
        config: {
          password,
        },
      },
    },
  }

  return await fetchOryService(
    `${process.env.KRATOS_ADMIN_URL}/admin/identities`,
    {
      body: JSON.stringify(identity),
      method: 'POST',
    },
  )
}
