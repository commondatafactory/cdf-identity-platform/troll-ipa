// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type CustomRelationTuple } from '../utils/permission-models'
import { fetchOryService } from '../utils/fetch-ory-service'

export const getPermissions = async (
  query: string,
  pageToken = '',
  allResults: CustomRelationTuple[] = [],
): Promise<CustomRelationTuple[]> => {
  const pageSize = 100

  const url = new URL(`${process.env.KETO_READ_URL}/relation-tuples?${query}`)
  url.searchParams.set('page_size', String(pageSize))

  if (pageToken) {
    url.searchParams.set('page_token', pageToken)
  }

  const data = (await fetchOryService(url.toString())) as {
    relation_tuples: CustomRelationTuple[]
    next_page_token: string
  }

  const updatedResults = [...allResults, ...data.relation_tuples]

  if (data.next_page_token === '' || !data.next_page_token) {
    return updatedResults
  }

  return await getPermissions(query, data.next_page_token, updatedResults)
}
