// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Identity } from '@ory/kratos-client'

import { fetchOryService } from '../utils/fetch-ory-service'

export const getUser = async ({
  userId,
}: {
  userId: string
}): Promise<Identity> =>
  await fetchOryService(
    `${process.env.KRATOS_ADMIN_URL}/admin/identities/${userId}`,
  )
