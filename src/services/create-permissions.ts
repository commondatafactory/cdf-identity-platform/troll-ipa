// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type CustomRelationTuple } from '../utils/permission-models'
import { fetchOryService } from '../utils/fetch-ory-service'

export const createPermissions = async (
  userId: string,
  permissions: Array<Partial<CustomRelationTuple>>,
) => {
  const createdPermissions = await Promise.all(
    permissions.map(async (permission) => {
      const relationTuple = {
        namespace: permission.namespace ?? 'permissions',
        object: permission.object,
        relation: permission.relation,
        [permission.subject_set ? 'subject_set' : 'subject_id']:
          permission.subject_set || permission.subject_id || userId,
      }

      return await fetchOryService(
        `${process.env.KETO_WRITE_URL}/admin/relation-tuples`,
        {
          body: JSON.stringify(relationTuple),
          method: 'PUT',
        },
      )
    }),
  )

  return createdPermissions
}
