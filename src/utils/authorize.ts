// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { getUserType } from './get-user-type'

// export * as authorize
// auth.isSameOrganization({permissionsA, permissionsB}, authModel)
// try {
//    if (
//        auth.isSameOrganization({ permissionsA, permissionsB }, authModel) &&
//        auth.isAdmin({ permissionsA }, authModel)
//    )
// }
// catch {
// log
// }

const isSameOrganization = ({ permissionsA, permissionsB }, authModel) => {
  // check if tuples have organization namespace
  // check if tuples have same organization name
  if (
    permissionsA.some(
      (permissionA) =>
        permissionA.namespace === authModel.organizations.namespace &&
        permissionsB.some(
          (permissionB) =>
            permissionB.namespace === authModel.organizations.namespace &&
            permissionA.object === permissionB.object,
        ),
    )
  ) {
    return true
  } else {
    throw ''
  }
}

const isAdmin = ({ permissionsA }, authModel) => {
  if (getUserType(permissionsA, authModel)) {
    return true
  } else {
    // voor de log
    throw ''
  }
}

export default { isSameOrganization, isAdmin }
