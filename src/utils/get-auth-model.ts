// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import type AuthorizationModel from '../authorization-model.json'

export const getContextAuthModel = async (): Promise<
  typeof AuthorizationModel
> => {
  console.log(
    `[${new Date().toLocaleString(undefined, { timeZone: 'UTC' })}] `,
    'Updating authorization model.',
  )

  let body: typeof AuthorizationModel

  if (process.env.NODE_ENV === 'development') {
    console.log('\x1b[2m', `From: local file`, '\x1b[0m')
    body = await import('../authorization-model.json')
  } else {
    console.log('\x1b[2m', `From: ${process.env.AUTH_MODELS_URL}`, '\x1b[0m')
    const response = await fetch(process.env.AUTH_MODELS_URL)
    body = (await response.json()) as typeof AuthorizationModel
  }

  console.log(
    '\x1b[2m',
    `Available themes: ${JSON.stringify(
      Object.values(body.subdivisions.objects),
    )
      .replaceAll('"', '')
      .replace(']', '')
      .replace('[', '')
      .replaceAll(',', ', ')}`,
    '\x1b[0m',
  )

  console.log(
    '\x1b[2m',
    `Available organizations: ${JSON.stringify(
      Object.values(body.organizations.objects),
    )
      .replaceAll('"', '')
      .replace(']', '')
      .replace('[', '')
      .replaceAll(',', ', ')}`,
    '\x1b[0m',
  )

  return body
}
