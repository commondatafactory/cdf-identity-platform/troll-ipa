// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

const formatDate = (date: Date) => {
  const year = date.getFullYear()
  const month = String(date.getMonth() + 1).padStart(2, '0')
  const day = String(date.getDate()).padStart(2, '0')
  const hours = String(date.getHours()).padStart(2, '0')
  const minutes = String(date.getMinutes()).padStart(2, '0')
  const seconds = String(date.getSeconds()).padStart(2, '0')

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}

export const LogStack = class LogStack {
  loggingServer: 'public' | 'private'
  finalLog: Record<string, string> = {}

  setLoggingServer(serverType) {
    this.loggingServer = serverType
  }

  audience(audience = 'audit') {
    this.finalLog = { ...this.finalLog, audience }
  }

  trace(log: Record<string, string>) {
    this.finalLog = { ...this.finalLog, ...log, type: 'trace' }
  }

  // TODO: When does this tag make sense?
  info(log: Record<string, string>) {
    this.finalLog = { ...this.finalLog, ...log }

    if (!this.finalLog.type) {
      this.finalLog = { ...this.finalLog, type: 'info' }
    }
  }

  warning(log: Record<string, string>) {
    this.finalLog = { ...this.finalLog, ...log, type: 'warning' }
  }

  error(log: Record<string, string>) {
    this.finalLog = { ...this.finalLog, ...log, type: 'error' }
  }

  log() {
    if (this.finalLog.audience) {
      // Note: Timestamp is recorded in seconds in preperation of FluentBit timestamp logging
      Object.assign(this.finalLog, { ts: Date.now() / 1000 })

      console.log(JSON.stringify(this.finalLog))
    } else {
      const date = formatDate(new Date(Date.now()))
      const type = this.finalLog.type
      const loggingServer = this.loggingServer

      delete this.finalLog.type

      const formattedLog = `[CDF authorization ${loggingServer}] [${date}] [${type}] ${Object.entries(
        this.finalLog,
      )
        .map(([key, value]) => `${key}=${value}`)
        .join(', ')}`

      console.log('\x1b[2m', formattedLog, '\x1b[0m')
    }
  }
}
