// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const isString = (item): item is string => {
  return typeof item === 'string'
}

export const isTuples = (tuples): tuples is any[] => {
  return (
    Array.isArray(tuples) &&
    tuples.every(
      ({ namespace, object, relation, subject_id, subject_set }) =>
        namespace && object && relation,
    )
  )
}
