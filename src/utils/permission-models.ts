// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import type authModelProps from '../authorization-model.json'

export type UserType = 'administrator' | 'analyst'
export type RelationType = 'access' | 'issuer' | 'admin'

export const userTypeLabels = [
  {
    id: 'administrator',
    type: 'user',
    label: 'Gemeentelijke administrator',
  },
  {
    id: 'analyst',
    type: 'user',
    label: 'Data-analist',
  },
]

export const organizationModels = (model) =>
  model.organizations.data.map((organization) => ({
    id: organization.objectId,
    label: organization.reference,
    type: 'organization',
  }))

export const availableRoleModels = (model: typeof authModelProps) =>
  model.organizations.data.reduce(
    (acc, organization) => [
      ...acc,
      ...organization.subdivisions.map((role) => ({
        id: `${role}_${organization.objectId}`,
        label: role.charAt(0).toUpperCase() + role.slice(1),
        type: 'role',
        roleId: role,
      })),
    ],
    [],
  )

export const organizationPermissions = (model) =>
  model.organizations.data.reduce(
    (acc, organization) => [
      ...acc,
      ...Object.values(model.organizations.relations).map((relation) => ({
        namespace: model.organizations.namespace,
        object: organization.objectId,
        relation,
      })),
    ],
    [],
  )

type Subject = any
// | {
//   subject_id?: never
//   subject_set: (typeof rolePermissions)[number]
// }
// | {
//   subject_id: string
//   subject_set?: never
// }

export type CustomRelationTuple = {
  namespace: string
  object: string
  relation: string
} & Subject
