// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const stripHtml = (htmlString: string) => {
  const plainText = htmlString.replace(/(<([^>]+)>)/gi, '')
  return plainText
}
