// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { CustomError } from '../errors/response-error'

export const userPassesTwoFactorCheck = ({
  assuranceLevel,
  userPermissions,
}) => {
  const shouldBeTwoFactorAuthenticated = userPermissions.some(
    (permission) =>
      permission.namespace === 'organizations' && permission.object !== 'dego',
  )

  if (!shouldBeTwoFactorAuthenticated) {
    return true
  }

  if (assuranceLevel !== 'aal2') {
    throw new CustomError({
      code: 403,
      message: 'Forbidden',
      debug: 'Not two factor authenticated but is :' + assuranceLevel,
    })
  }

  return true
}
