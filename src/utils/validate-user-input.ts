// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

// [\p{L}0-9\s]+: Matches one or more occurrences of any Unicode letter (\p{L}), digit (0-9), or whitespace character (\s).
export const isValidUserInput = ({
  input,
  maxLength = 30,
  minLength = 2,
}: {
  input: string
  maxLength?: number
  minLength?: number
}): boolean => {
  if (input.length < minLength || input.length > maxLength) {
    return false
  }

  const regex = /^[\p{L}0-9\s]+$/u
  return regex.test(input)
}

export const isValidPassword = ({
  password,
  minLength = 2,
  maxLength = 30,
}: {
  password: string
  maxLength?: number
  minLength?: number
}) => {
  if (password.length < minLength || password.length > maxLength) {
    return false
  }

  const regex = /^[^\s]{8,}$/
  return regex.test(password)
}
