// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type GenericError } from '@ory/kratos-client'

import { CustomError } from '../errors/response-error'

export const fetchOryService = async (
  input: string | URL,
  init: RequestInit = {},
): Promise<any> => {
  try {
    const response = await fetch(input, init)

    if (!response.ok) {
      const body: GenericError = await response?.json()
      const error: GenericError = body.error

      if (!error) {
        throw new CustomError({ code: 500, message: response.statusText })
      }

      throw new CustomError(error)
    }

    const contentType = response.headers.get('content-type')
    if (contentType && contentType.includes('application/json')) {
      return await response.json()
    }
    return {}
  } catch (error) {
    console.log('Full error', error)
    throw new CustomError(error)
  }
}


export const fetchKratosService = async (
  input: string | URL ,
  init: RequestInit = {},
): Promise<any> => {
  try {
    let allData: any[] = [];
    let url: string | URL | null;

    url = input
    url = `${process.env.KRATOS_ADMIN_URL}${url}`

    while(url) {
        const response = await fetch(url, init)
        if (!response.ok) {
          const body: GenericError = await response?.json()
          const error: GenericError = body.error
          if (!error) {
            throw new CustomError({ code: 500, message: response.statusText })
          }
          throw new CustomError(error)
        }

        const contentType = response.headers.get('content-type')
        if (contentType && contentType.includes('application/json')) {
          const data = await response.json()
          allData = allData.concat(data)
        }

        let linkHeader: string | null = response.headers.get('Link');
        url = getNextPageUrl(linkHeader);
        if(url) {
            url = `${process.env.KRATOS_ADMIN_URL}${url}`
        }
    }

    return allData

  } catch (error) {
    console.log('Full error', error)
    throw new CustomError(error)
  }
}

function getNextPageUrl(linkHeader: string | null): string | null {
    if (!linkHeader) return null;

    const links = linkHeader.split(',').map(link => link.trim());
    for (let link of links) {
        const match = link.match(/<([^>]+)>;\s*rel="next"/);
        if (match) return match[1];
    }
    return null;
}
