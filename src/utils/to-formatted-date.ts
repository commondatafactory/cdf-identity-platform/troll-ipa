// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const toFormattedDate = (ISO8601string) => {
  const date = new Date(ISO8601string)

  const dateOptions: Intl.DateTimeFormatOptions = {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  }

  const timeOptions: Intl.DateTimeFormatOptions = {
    hour: 'numeric',
    minute: 'numeric',
    hour12: false,
  }

  const formattedDate = date.toLocaleDateString('nl-NL', dateOptions)
  const formattedTime = date.toLocaleTimeString('nl-NL', timeOptions)

  const formattedOutput = `${formattedDate} om ${formattedTime} uur`

  return formattedOutput
}
