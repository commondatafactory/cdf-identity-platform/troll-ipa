// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type CustomRelationTuple, type UserType } from './permission-models'

import type authorizationModel from '../authorization-model.json'

// NOTE: Rather than refetching permissions, we're searching through fetched permissions to determine user type
export const getUserType = (
  permissions: CustomRelationTuple[],
  authModel: typeof authorizationModel,
): UserType => {
  if (
    permissions.some(
      (permission) =>
        permission.namespace === authModel.organizations.namespace &&
        permission.relation === authModel.organizations.relations.admin,
    )
  ) {
    return 'administrator'
  }

  return 'analyst'
}
