// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import sgMail from '@sendgrid/mail'
import type Mail from 'nodemailer/lib/mailer'

export const sendMail = async (
  mailData: Omit<sgMail.MailDataRequired, 'from'> | Mail.Options,
): Promise<any> => {
  try {
    // import nodemailer from "nodemailer";

    if (process.env.NODE_ENV === 'development') {
      const nodemailer = await import('nodemailer')
      // NOTE: test mails can be found at https://ethereal.email/messages

      const testAccount = await nodemailer.createTestAccount()

      const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
          user: testAccount.user,
          pass: testAccount.pass,
        },
      })

      const info = await transporter.sendMail({
        from: {
          name: 'vip',
          address: 'vip@vng.nl',
        },
        ...(mailData as Mail.Options),
      })

      console.log('Message sent: %s', info.messageId)

      // Preview only available when sending through an Ethereal account
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))

      return info
    }

    const updatedMailData: Partial<sgMail.MailDataRequired> = {
      from: {
        name: 'vip',
        email: 'vip@vng.nl',
      },
      ...(mailData as sgMail.MailDataRequired),
    }

    sgMail.setApiKey(process.env.SENDGRID_API_KEY)

    const mail = sgMail
      .send(updatedMailData)
      .then((response) => {
        return response
      })
      .catch((error) => {
        console.error('Send mail error: ', error)
        return error
      })

    return await mail
  } catch (e) {
    console.log('send-mail error:', e)
  }
}
