// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Identity, type Session } from '@ory/kratos-client'

export interface SessionWithIdentity extends Session {
  identity: Identity
}
