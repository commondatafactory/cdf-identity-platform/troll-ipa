// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { createPermissions } from '../services/create-permissions'
import { currentUser } from '../services/get-current-session'
import type authModelProps from '../authorization-model.json'
import { type CustomRelationTuple } from '../utils/permission-models'
import { deletePermissions } from '../services/delete-permissions'
import { getPermissions } from '../services/get-permissions'
import { isString, isTuples } from '../utils/validators'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import { CustomError } from '../errors/response-error'

const authorizeRequest = ({
  loggedInUserId,
  loggedInUserPermissions,
  requestedTuples,
  targetUserTuples,
  userToUpdateId,
  authModel,
}: {
  loggedInUserId: string
  loggedInUserPermissions: Array<Partial<CustomRelationTuple>>
  requestedTuples: Array<{ namespace; object; relation }>
  targetUserTuples: Array<Partial<CustomRelationTuple>>
  userToUpdateId: string
  authModel: typeof authModelProps
}) => {
  const { organizations, subdivisions } = authModel

  return requestedTuples.every((requestedTuple) => {
    // NOTE: if tuple is for requesting user
    if (loggedInUserId === userToUpdateId) {
      // Is requested tuple a possible tuple, that's all
      const isRequestedTuplePermittedRole =
        requestedTuple.namespace === subdivisions.namespace &&
        (requestedTuple.relation === subdivisions.relations.issuer ||
          requestedTuple.relation === organizations.relations.access)

      const isRequestedTuplePermittedOrganization =
        requestedTuple.namespace === organizations.namespace &&
        [
          organizations.relations.access,
          organizations.relations.issuer,
          organizations.relations.admin,
        ].includes(requestedTuple.relation)

      if (
        !isRequestedTuplePermittedRole &&
        !isRequestedTuplePermittedOrganization
      ) {
        // consoleSequential({
        //   authorized: [
        //     'Forbidden:',
        //     'logged in user:',
        //     'isRequestedTuplePermittedRole',
        //     'isRequestedTuplePermittedOrganization',
        //   ],
        // })
        return
      }

      // Is logged in user - allowed to grant - requested permission - to themselves
      const hasNecessaryRights = loggedInUserPermissions.some(
        (loggedInUserTuple) =>
          requestedTuple.object === loggedInUserTuple.object &&
          ((loggedInUserTuple.namespace === subdivisions.namespace &&
            loggedInUserTuple.relation === subdivisions.relations.issuer) ||
            (loggedInUserTuple.namespace === organizations.namespace &&
              loggedInUserTuple.relation === organizations.relations.issuer)),
      )

      if (!hasNecessaryRights) {
        // consoleSequential({
        //   logs: ['Forbidden:', 'logged in user:', 'hasNecessaryRights'],
        // })
        return
      }

      return true
    }

    // If tuple is for other user than requesting user
    if (loggedInUserId !== userToUpdateId) {
      const targetUserOrganizations = targetUserTuples.filter(
        (tuple) => tuple.namespace === organizations.namespace,
      )
      const loggedInUserOrganizations = loggedInUserPermissions.filter(
        (tuple) => tuple.namespace === organizations.namespace,
      )

      // Is logged in user - in same organization - and - is admin - and - isn't target user an admin
      const hasAccessToTargetUser = loggedInUserOrganizations.some(
        (loggedInUserOrganization) =>
          loggedInUserOrganization.relation === organizations.relations.admin &&
          targetUserOrganizations.some(
            (targetUserOrganization) =>
              targetUserOrganization.object ===
                loggedInUserOrganization.object &&
              targetUserOrganization.relation !== organizations.relations.admin,
          ),
      )

      if (!hasAccessToTargetUser) {
        // consoleSequential({
        //   logs: ['Forbidden:', 'other user:', 'hasAccessToTargetUser'],
        // })
        return
      }

      // Is requested permission - a permission that's allowed to be given
      const isPermissionWithinLimit = requestedTuples.every(
        (requestedTuple) =>
          (requestedTuple.namespace === subdivisions.namespace &&
            requestedTuple.relation === subdivisions.relations.issuer) ||
          (requestedTuple.namespace === organizations.namespace &&
            requestedTuple.relation === organizations.relations.issuer),
      )

      if (!isPermissionWithinLimit) {
        // consoleSequential({
        //   logs: ['Forbidden:', 'other user:', 'isPermissionWithinLimit'],
        // })
        return
      }

      // Does logged in user - have rights - to the requested permission
      const isPermissionAllowedToGrant = requestedTuples.every(
        (requestedTuple) =>
          loggedInUserPermissions.some(
            (requestingUserTuple) =>
              requestedTuple.object === requestingUserTuple.object &&
              requestingUserTuple.namespace === subdivisions.namespace &&
              requestingUserTuple.relation === subdivisions.relations.issuer,
          ),
      )

      if (!isPermissionAllowedToGrant) {
        // consoleSequential({
        //   logs: ['Forbidden:', 'other user:', 'isPermissionAllowedToGrant'],
        // })
        return
      }

      return true
    }
  })
}

export default async ({
  request,
  response,
  params,
  authorizationModel,
  logStack,
}: Context) => {
  logStack.audience()

  const requestedPermissions: CustomRelationTuple[] = request.body
  const userToUpdateId = params?.id
  const cookie = request.headers.cookie

  const isValidRequest =
    isTuples(requestedPermissions) &&
    isString(userToUpdateId) &&
    isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      message: 'Invalid request',
      code: 400,
      debug: `tuples: ${requestedPermissions.join(', ')}, userId: ${userToUpdateId}`,
    })
  }

  const { loggedInUserSession, loggedInUserType, loggedInUserPermissions } =
    await currentUser(cookie, authorizationModel)

  logStack.info({ user_identity: loggedInUserSession.identity.id })

  const isTwoFactorAuthenticated = userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  if (!isTwoFactorAuthenticated) {
    throw new CustomError({
      message: 'Forbidden',
      code: 403,
      debug: `Two factor authentication: ${isTwoFactorAuthenticated}`,
    })
  }

  const query = new URLSearchParams({
    subject_id: userToUpdateId,
  }).toString()

  const updateUserPermissions = await getPermissions(query)

  const isAuthorized = authorizeRequest({
    loggedInUserId: loggedInUserSession.identity.id,
    loggedInUserPermissions,
    requestedTuples: requestedPermissions,
    targetUserTuples: updateUserPermissions,
    userToUpdateId,
    authModel: authorizationModel,
  })

  if (!isAuthorized) {
    throw new CustomError({
      message: 'Forbidden',
      code: 403,
      debug: `Is Authorized: ${isAuthorized}`,
    })
  }

  // preemptively delete permissions with access
  if (
    requestedPermissions.some(
      (permission) =>
        permission.namespace === authorizationModel.organizations.namespace,
    )
  ) {
    const tuple: Partial<CustomRelationTuple> = {
      namespace: authorizationModel.organizations.namespace,
      relation: authorizationModel.organizations.relations.access,
      subject_id: userToUpdateId,
    }
    await deletePermissions(userToUpdateId, tuple)

    logStack.info({ deletedOrganizationsPermissions: tuple })
  }

  if (
    requestedPermissions.some(
      (permission) =>
        permission.namespace === authorizationModel.subdivisions.namespace,
    )
  ) {
    const tuple: Partial<CustomRelationTuple> = {
      namespace: authorizationModel.subdivisions.namespace,
      relation: authorizationModel.subdivisions.relations.access,
      subject_id: userToUpdateId,
    }
    await deletePermissions(userToUpdateId, tuple)

    logStack.info({ deletedSubdivisionsPermissions: tuple })
  }

  const updatePermissions: Array<Partial<CustomRelationTuple>> =
    requestedPermissions.map((permission) => ({
      namespace: permission.namespace,
      object: permission.object,
      relation: permission.relation,
      subject_id: userToUpdateId,
    }))

  const createdPermissions = await createPermissions(
    userToUpdateId,
    updatePermissions,
  )

  logStack.info({ createdPermissions })

  response.status = 200
  response.body = createdPermissions
}
