// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { currentUser } from '../services/get-current-session'
import { CustomError } from '../errors/response-error'
import { getUserByField } from '../services/get-user-by-field'
import { isString } from '../utils/validators'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import { type UserType } from '../utils/permission-models'

const authorizeRequest = ({
  loggedInUserType,
}: {
  loggedInUserType: UserType
}) => {
  return loggedInUserType === 'administrator'
}

export default async ({ request, response, authorizationModel }: Context) => {
  const cookie = request.headers.cookie
  const email = request.query.email

  const isValidRequest = isString(email) && isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `email is string: ${isString(email)}`,
    })
  }

  const { loggedInUserSession, loggedInUserPermissions, loggedInUserType } =
    await currentUser(cookie, authorizationModel)

  userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  const isAuthorized = authorizeRequest({ loggedInUserType })

  if (!isAuthorized) {
    response.status = 403
    response.body = { error: 'forbidden', message: '' }
    return
  }

  if (await getUserByField({ email })) {
    response.status = 204
    return
  }

  response.status = 404
}
