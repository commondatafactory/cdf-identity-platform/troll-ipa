// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import Router from '@koa/router'

// import createAccount from './root/create-account'
import deletePermission from './delete-permission'
import deleteUser from './delete-user'
import getConfiguredCredentials from './get-configured-credentials'
import getPermissions from './get-permissions'
import getUsers from './get-users'
import headUsers from './head-users'
import healthCheck from './root/health-check'
import passwordRecovery from './password-recovery'
import postUser from './post-user'
import putPermissionsUserId from './put-permissions-user-id'
import putUsersId from './put-users-id'
import whoami from './whoami'
import degoPermissions from './dego-permissions'

const root = new Router()
const permissions = new Router()
const users = new Router()

root.get('/', healthCheck)
root.get('/api', healthCheck)
// .post('/create-account', createAccount)

permissions
  .prefix('/api/permissions')
  .get('/', getPermissions)
  .delete('/', deletePermission)
  .put('/:id', putPermissionsUserId)
  .get('/dego', degoPermissions)

users
  .prefix('/api/users')
  .get('/whoami', whoami)
  .get('/configured-credentials', getConfiguredCredentials)
  .get('/password-recovery', passwordRecovery)
  .head('/', headUsers) // NOTE: should come before GET as it will be overwritten
  .get('/', getUsers)
  .delete('/', deleteUser)
  .put('/:id', putUsersId)
  .post('/', postUser)

export default { permissions, users, root }
