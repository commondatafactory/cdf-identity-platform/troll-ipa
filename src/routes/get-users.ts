// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { currentUser } from '../services/get-current-session'
import { getUserGroup } from '../services/get-user-group'
import { getUserByField } from '../services/get-user-by-field'
import { isString } from '../utils/validators'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import { CustomError } from '../errors/response-error'

const authorizeRequest = ({
  loggedInUserType,
  email,
  loggedInUserPermissions,
  organization,
  authModel,
}) => {
  if (email) {
    if (loggedInUserType === 'administrator') {
      return true
    }
  }

  if (
    !loggedInUserPermissions.some(
      (permission) =>
        permission.namespace === authModel.organizations.namespace &&
        permission.object === organization &&
        permission.relation === 'access',
    )
  ) {
    return false
  }

  return true
}

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  const cookie = request.headers.cookie
  const { namespace, object: userOrganization, relation, email } = request.query

  const isValidRequest =
    (!namespace || isString(namespace)) &&
    (!userOrganization || isString(userOrganization)) &&
    (!relation || isString(relation)) &&
    (!email || isString(email)) &&
    isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `namespace is string: ${isString(
        namespace,
      )}, userOrganization is string: ${isString(userOrganization)}, relation is string: ${isString(
        relation,
      )}, email is string: ${isString(email)}`,
    })
  }

  const { loggedInUserSession, loggedInUserType, loggedInUserPermissions } =
    await currentUser(cookie, authorizationModel)

  logStack.info({ user_identity: loggedInUserSession.identity.id })

  userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  const isAuthorized = authorizeRequest({
    loggedInUserType,
    email,
    loggedInUserPermissions,
    organization: userOrganization,
    authModel: authorizationModel,
  })

  if (!isAuthorized) {
    throw new CustomError({
      code: 403,
      message: 'Forbidden',
      debug: 'Authorization failed',
    })
  }

  const users = userOrganization
    ? await getUserGroup(userOrganization, relation, null, authorizationModel)
    : [await getUserByField({ email })]

  if (!users) {
    throw 'Not able to get requested users'
  }

  response.status = 200
  response.body = users
}
