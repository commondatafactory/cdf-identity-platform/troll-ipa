// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { currentUser } from '../services/get-current-session'
import { type CustomRelationTuple } from '../utils/permission-models'
import { deletePermissions } from '../services/delete-permissions'
import { getPermissions } from '../services/get-permissions'
import { isString } from '../utils/validators'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import { CustomError } from '../errors/response-error'
import type authModelProps from '../authorization-model.json'

const authorizeRequest = async ({
  loggedInUserId,
  loggedInUserPermissions,
  userToUpdateId,
  authModel,
}: {
  loggedInUserId: string
  loggedInUserPermissions: Array<Partial<CustomRelationTuple>>
  userToUpdateId: string
  authModel: typeof authModelProps
}) => {
  const { organizations } = authModel

  // Is tuple - for requesting user
  if (loggedInUserId === userToUpdateId) {
    return true
  }

  // // Getting loggedInUser active organization tuple
  const loggedInUserAdminOrganizations = loggedInUserPermissions.filter(
    (permission) =>
      permission.namespace === organizations.namespace &&
      permission.relation === organizations.relations.admin,
  )

  // // get user to delete permissions
  const query = new URLSearchParams({
    namespace: organizations.namespace,
    subject_id: userToUpdateId,
  }).toString()

  const userToUpdatePermissions = await getPermissions(query)

  // // Getting userToUpdate organizations
  const userToUpdateOrganizations = userToUpdatePermissions.filter(
    (permission) =>
      permission.namespace === organizations.namespace &&
      permission.relation === organizations.relations.issuer,
  )

  // Is loggedInUser admin - for same - userToUpdate organization
  const loggedInUserIsAdminOverSharedOrganization =
    loggedInUserAdminOrganizations.find((loggedInUserAdminOrganization) =>
      userToUpdateOrganizations.some(
        (usertoUpdateOrganization) =>
          loggedInUserAdminOrganization.object ===
          usertoUpdateOrganization.object,
      ),
    )

  if (!loggedInUserIsAdminOverSharedOrganization) {
    return false
  }

  // Is userToUpdate - deletable
  const isUserToUpdateChangable = !userToUpdateOrganizations.some(
    (organization) =>
      organization.object ===
        loggedInUserIsAdminOverSharedOrganization.object &&
      organization.relation === organizations.relations.admin,
  )

  if (!isUserToUpdateChangable) {
    return false
  }

  return true
}

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  logStack.audience()

  const { namespace, object, relation, subject_id } = request.query
  const cookie = request.headers.cookie

  const isValidRequest =
    (!namespace || isString(namespace)) &&
    (!object || isString(object)) &&
    (!relation || isString(relation)) &&
    isString(subject_id) &&
    isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `namespace is string: ${isString(
        namespace,
      )}, object is string: ${isString(object)}, relation is string: ${isString(
        relation,
      )}, subject_id is string: ${isString(subject_id)}`,
    })
  }

  const { loggedInUserSession, loggedInUserType, loggedInUserPermissions } =
    await currentUser(cookie, authorizationModel)

  logStack.info({ identity_id: loggedInUserSession.identity.id })

  userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  const isAuthorized = await authorizeRequest({
    loggedInUserId: loggedInUserSession.identity.id,
    loggedInUserPermissions,
    userToUpdateId: subject_id,
    authModel: authorizationModel,
  })

  if (!isAuthorized) {
    throw new CustomError({
      code: 403,
      message: 'Forbidden',
      debug: 'Authorization failed',
    })
  }

  const tuple = {} as CustomRelationTuple

  if (namespace) tuple.namespace = namespace
  if (object) tuple.object = object
  if (relation) tuple.relation = relation
  if (subject_id) tuple.subject_id = subject_id

  await deletePermissions(loggedInUserSession.identity.id, tuple)

  logStack.info({ tuple_query: tuple })

  response.status = 204
  response.message = 'Deleted permissions'
}
