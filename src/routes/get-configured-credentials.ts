// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { currentUser } from '../services/get-current-session'
import { CustomError } from '../errors/response-error'
import { isString } from '../utils/validators'
import { getUser } from 'src/services/get-user'

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  const cookie = request.headers.cookie

  const isValidRequest = isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: ``,
    })
  }

  const { loggedInUserSession } = await currentUser(cookie, authorizationModel)

  logStack.info({ user_identity: loggedInUserSession.identity.id })

  const userId = loggedInUserSession.identity.id

  if (!userId) {
    throw new CustomError({
      code: 500,
      message: 'Invalid request',
      debug: `User not found`,
    })
  }

  const userIdentityBody = await getUser({ userId })

  const configuredPassword = !!userIdentityBody.credentials?.password
  const configuredTotp = !!userIdentityBody.credentials?.totp

  response.status = 200
  response.body = { password: configuredPassword, totp: configuredTotp }
}
