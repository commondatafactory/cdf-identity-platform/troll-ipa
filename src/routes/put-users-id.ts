// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Identity } from '@ory/kratos-client'
import { type Context } from 'koa'

import { currentUser } from '../services/get-current-session'
import { CustomError } from '../errors/response-error'
import { getPermissions } from '../services/get-permissions'
import { getUser } from '../services/get-user'
import { isString } from '../utils/validators'
import { mailEditedName } from '../custom-mails/mail-edited-name'
import { mailLayoutHTML } from '../custom-mails/mail-layout'
import { sendMail } from '../utils/send-mail'
import { updateUser } from '../services/update-user'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import authModelProps from '../authorization-model.json'

const authorizeRequest = ({
  loggedInUserType,
  loggedInUserPermissions,
  userToUpdatePermissions,
  authModel,
}) => {
  const isSameMunicipality = loggedInUserPermissions.some(
    (loggedInUserPermission) =>
      loggedInUserPermission.namespace === 'organizations' &&
      userToUpdatePermissions.some(
        (userToUpdatePermission) =>
          userToUpdatePermission.namespace ===
            authModel.organizations.namespace &&
          loggedInUserPermission.object === userToUpdatePermission.object,
      ),
  )

  const hasAuthorizedRole = ['administrator'].includes(loggedInUserType)

  return isSameMunicipality && hasAuthorizedRole
}

export default async ({
  request,
  response,
  params,
  authorizationModel,
  logStack,
}: Context) => {
  logStack.audience()

  const userUpdateBodyRequest: Partial<Identity> = request.body
  const cookie = request.headers.cookie
  const userToUpdateId = params?.id

  const isValidRequest =
    typeof userUpdateBodyRequest === 'object' &&
    isString(cookie) &&
    isString(userToUpdateId)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: ``,
    })
  }

  const { loggedInUserType, loggedInUserPermissions, loggedInUserSession } =
    await currentUser(cookie, authorizationModel)

  logStack.info({ user_identity: loggedInUserSession.identity.id })

  userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  const userToUpdate = await getUser({ userId: userToUpdateId })

  const tuple = {
    subject_id: userToUpdate.id,
  }
  const query = new URLSearchParams(tuple).toString()
  const userToUpdatePermissions = await getPermissions(query)

  const isAuthorized = authorizeRequest({
    loggedInUserType,
    loggedInUserPermissions,
    userToUpdatePermissions,
    authModel: authorizationModel,
  })

  if (!isAuthorized) {
    throw new CustomError({
      code: 403,
      message: 'Forbidden',
      debug: 'Authorization failed',
    })
  }

  const updatedUser = await updateUser({
    userId: userToUpdate.id,
    userData: {
      ...userToUpdate,
      ...userUpdateBodyRequest,
      credentials: undefined,
    },
  })

  // NOTE: Kratos GET user output werkt niet meer als input voor nieuwe gebruiker; de credentials gaan mis. Wellicht waard om te testen of credentials als undefined versturen, of kan ook PATCH gebruiken nu:
  // https://github.com/ory/kratos/issues/2944

  const receiver = {
    name: {
      first: updatedUser.traits.name.first,
      last: updatedUser.traits.name.last,
    },
    previousName: {
      first: userToUpdate.traits.name.first,
      last: userToUpdate.traits.name.last,
    },
    mail: updatedUser.traits.email,
  }
  const sender = {
    name: {
      first: loggedInUserSession.identity.traits.name.first,
      last: loggedInUserSession.identity.traits.name.last,
    },
    mail: loggedInUserSession.identity.traits.email,
  }

  if (
    userToUpdate.traits.name.first !== updatedUser.traits.name.first ||
    userToUpdate.traits.name.last !== updatedUser.traits.name.last
  ) {
    const nameEdit = mailEditedName({
      receiver,
      sender,
      loginUrl: String(process.env.ANALYST_LOGIN_URL),
    })
    await sendMail({
      to: receiver.mail,
      subject: nameEdit.subject,
      html: mailLayoutHTML({
        title: nameEdit.subject,
        content: nameEdit.html,
      }),
    })
  }

  logStack.info({
    mail: '✅',
  })

  response.status = 200
  response.body = updatedUser
}
