// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { type Identity } from '@ory/kratos-client'
import { type Context } from 'koa'

import {
  type CustomRelationTuple,
  organizationModels,
} from '../utils/permission-models'
import { generatePassword } from '../utils/generate-password'
import { createUser } from '../services/create-user'
import { sendMail } from '../utils/send-mail'
import { createPermissions } from '../services/create-permissions'
import { mailLayoutHTML } from '../custom-mails/mail-layout'
import { mailAdminAccountInvitation } from '../custom-mails/account-admin-invitation'
import { createRecoveryCode } from '../services/create-recovery-code'
import { toFormattedDate } from '../utils/to-formatted-date'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import { currentUser } from '../services/get-current-session'
import { CustomError } from '../errors/response-error'
import { isString, isTuples } from '../utils/validators'
import type authModelProps from '../authorization-model.json'
import {
  isValidPassword,
  isValidUserInput,
} from 'src/utils/validate-user-input'

const authorizeRequest = ({
  requestedTuples,
  loggedInUserPermissions,
  authModel,
}: {
  requestedTuples: any[]
  loggedInUserPermissions: any[]
  authModel: typeof authModelProps
}) => {
  const { organizations, subdivisions } = authModel

  const loggedInUserOrganizations = loggedInUserPermissions.filter(
    (tuple) => tuple.namespace === organizations.namespace,
  )
  const requestedOrganizations = requestedTuples.filter(
    (tuple) => tuple.namespace === organizations.namespace,
  )

  // logged in user - is allowed - to grant requested organizations
  const hasAccessToRequestedOrganizations = loggedInUserOrganizations.some(
    (userOrganization) =>
      requestedOrganizations.every(
        (requestedOrganization) =>
          requestedOrganization.object === userOrganization.object &&
          userOrganization.relation === organizations.relations.admin,
      ),
  )

  if (!hasAccessToRequestedOrganizations) {
    return
  }

  // Is requested permission - a permission that's allowed to be given
  const isRequestedPermissionsWithinLimit = requestedTuples.every(
    (requestedTuple) =>
      (requestedTuple.namespace === subdivisions.namespace &&
        requestedTuple.relation === subdivisions.relations.issuer) ||
      (requestedTuple.namespace === organizations.namespace &&
        requestedTuple.relation === organizations.relations.issuer),
  )

  if (!isRequestedPermissionsWithinLimit) return

  // Does logged in user - have rights - to the requested role
  const isPermissionAllowedToGrant = requestedTuples
    .filter(
      (requestedTuple) => requestedTuple.namespace === subdivisions.namespace,
    )
    .every((requestedTuple) =>
      loggedInUserPermissions.some(
        (requestingUserTuple) =>
          requestedTuple.object === requestingUserTuple.object &&
          requestingUserTuple.relation === subdivisions.relations.issuer,
      ),
    )

  if (!isPermissionAllowedToGrant) return

  return true
}

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  logStack.audience()

  const grantPermissions: CustomRelationTuple[] = request.body.permissions || []
  const cookie = request.headers.cookie
  const createIdentity: Identity['traits'] & { password: string } =
    request.body.traits

  const isValidRequest =
    isTuples(grantPermissions) &&
    isValidUserInput({ input: createIdentity.name.first }) &&
    isValidUserInput({ input: createIdentity.name.last }) &&
    createIdentity.password
      ? isValidPassword({ password: createIdentity.password })
      : true

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `User is invalid`,
    })
  }

  const isValidCookie = isString(cookie)

  if (!isValidCookie) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `Cookies are invalid`,
    })
  }

  const { loggedInUserSession, loggedInUserType, loggedInUserPermissions } =
    await currentUser(cookie, authorizationModel)

  logStack.info({ user_identity: loggedInUserSession.identity.id })

  userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  const isAuthorized = authorizeRequest({
    requestedTuples: grantPermissions,
    loggedInUserPermissions,
    authModel: authorizationModel,
  })

  if (!isAuthorized) {
    throw new CustomError({
      code: 403,
      message: 'Forbidden',
      debug: 'Authorization failed',
    })
  }

  const newCreatedUser = await createUser({
    traits: {
      email: createIdentity.email,
      name: {
        first: createIdentity.name.first,
        last: createIdentity.name.last,
      },
    },
    password: createIdentity.password || generatePassword(),
    verified: true,
  })

  logStack.info({ new_user_identity: newCreatedUser.id })

  const recoveryData = await createRecoveryCode({ id: newCreatedUser.id })

  const createdPermissions = await createPermissions(
    newCreatedUser.id,
    grantPermissions,
  )

  logStack.info({
    new_created_identity: newCreatedUser.id,
    new_created_permissions: createdPermissions,
  })

  const loggedInUserOrganization = organizationModels(authorizationModel).find(
    (model) =>
      loggedInUserPermissions.some(
        (permission) => model.id === permission.object,
      ),
  )?.label

  const mailData = {
    receiver: {
      name: {
        first: newCreatedUser.traits.name.first,
        last: newCreatedUser.traits.name.last,
      },
      mail: newCreatedUser.traits.email,
    },
    sender: {
      name: {
        presented: `${newCreatedUser.traits.name.first} ${newCreatedUser.traits.name.last}`,
      },
    },
    service: 'DOOK',
    organization: loggedInUserOrganization, // TODO: possibly undefined
    expirationDate: toFormattedDate(recoveryData.expires_at),
    verificationCode: recoveryData.recovery_code,
    verificationUrl: recoveryData.recovery_link,
    loginUrl: process.env.ANALYST_LOGIN_URL.replace('https://', ''),
  }

  const accountInvitation = mailAdminAccountInvitation(mailData)
  await sendMail({
    to: newCreatedUser.traits.email,
    subject: accountInvitation.subject,
    html: mailLayoutHTML({
      title: accountInvitation.subject,
      content: accountInvitation.html,
    }),
  })

  logStack.info({
    mail: '✅',
  })

  response.status = 204
}
