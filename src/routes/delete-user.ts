// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { currentUser } from '../services/get-current-session'
import { getUser } from '../services/get-user'
import { getPermissions } from '../services/get-permissions'
import { deleteUser } from '../services/delete-user'
import { isString } from '../utils/validators'
import { userPassesTwoFactorCheck } from '../utils/user-passes-two-factor-check'
import { CustomError } from '../errors/response-error'

const authorizeRequest = async ({
  loggedInUserType,
  loggedInUserPermissions,
  toDeleteUserPermissions,
  authModel,
}) => {
  const isSameMunicipality = loggedInUserPermissions.some(
    (loggedInUserPermission) =>
      loggedInUserPermission.namespace === authModel.organizations.namespace &&
      toDeleteUserPermissions.some(
        (toDeleteUserPermission) =>
          toDeleteUserPermission.namespace ===
            authModel.organizations.namespace &&
          loggedInUserPermission.object === toDeleteUserPermission.object,
      ),
  )

  const hasAuthorizedRole = ['administrator'].includes(loggedInUserType)

  return isSameMunicipality && hasAuthorizedRole
}

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  logStack.audience()

  const cookie = request.headers.cookie
  const userId = request.query.id

  const isValidRequest = isString(userId) && isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `userId is string: ${isString(userId)}`,
    })
  }
  const { loggedInUserSession, loggedInUserType, loggedInUserPermissions } =
    await currentUser(cookie, authorizationModel)

  userPassesTwoFactorCheck({
    assuranceLevel: loggedInUserSession.authenticator_assurance_level,
    userPermissions: loggedInUserPermissions,
  })

  const toDeleteUser = await getUser({ userId })

  const tuple = {
    subject_id: toDeleteUser.id,
  }
  const query = new URLSearchParams(tuple).toString()
  const toDeleteUserPermissions = await getPermissions(query)

  const isAuthorized = authorizeRequest({
    loggedInUserType,
    loggedInUserPermissions,
    toDeleteUserPermissions,
    authModel: authorizationModel,
  })

  if (!(await isAuthorized)) {
    throw new CustomError({
      code: 403,
      message: 'Forbidden',
      debug: 'Authorization failed',
    })
  }

  await deleteUser({ userId: toDeleteUser.id })

  response.status = 204
}
