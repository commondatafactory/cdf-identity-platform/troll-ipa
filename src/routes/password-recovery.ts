// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { getUserByField } from '../services/get-user-by-field'
import { updateUser } from '../services/update-user'
import { mailPasswordRecovery } from '../custom-mails/mail-password-recovery'
import { sendMail } from '../utils/send-mail'
import { mailLayoutHTML } from '../custom-mails/mail-layout'
import { createRecoveryCode } from '../services/create-recovery-code'
import { CustomError } from '../errors/response-error'
import { isString } from '../utils/validators'

export default async ({ request, response, logStack }: Context) => {
  logStack.audience()

  const mailToRecover = request.query.email

  const isValidRequest = isString(mailToRecover)

  if (!isValidRequest) {
    throw new CustomError({
      code: 400,
      message: 'Invalid request',
      debug: `E-mail is string: ${isString(mailToRecover)}`,
    })
  }

  const userIdentityToRecover = await getUserByField({ email: mailToRecover })

  if (!userIdentityToRecover) {
    logStack.warning({ user_identity: 'No user found.' })
    response.status = 204
    return
  }

  logStack.info({ user_identity: userIdentityToRecover.id })

  const { credentials: _, ...userWithoutCredentials } = userIdentityToRecover

  const updatedUser = await updateUser({
    userId: userIdentityToRecover.id,
    userData: userWithoutCredentials,
  })

  const recoveryData = await createRecoveryCode({ id: updatedUser.id })

  const expirationDate = recoveryData.expires_at
    ? new Date(recoveryData.expires_at).toLocaleDateString('nl-NL', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
      })
    : '-'

  const mailData = {
    receiver: {
      name: {
        first: updatedUser.traits.name.first || '',
        last: updatedUser.traits.name.last || '',
      },
      mail: updatedUser.traits.email,
    },
    loginUrl: process.env.ANALYST_LOGIN_URL,
    expirationDate,
    verificationCode: recoveryData.recovery_code,
    verificationUrl: `${recoveryData.recovery_link}&screen=password-recovery`,
  }

  const passwordRecovery = mailPasswordRecovery(mailData)

  await sendMail({
    to: mailData.receiver.mail,
    subject: passwordRecovery.subject,
    html: mailLayoutHTML({
      title: passwordRecovery.subject,
      content: passwordRecovery.html,
    }),
  })

  response.status = 204
}
