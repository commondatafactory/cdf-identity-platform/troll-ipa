// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { currentUser } from '../services/get-current-session'
import { isString } from '../utils/validators'
import { CustomError } from '../errors/response-error'
import { type Context } from 'koa'

import AcceptedExternalMails from '../accepted-external-mails.json'
import AcceptedInternalMails from '../accepted-internal-mails.json'

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  const cookie = request.headers.cookie

  const isValidRequest = isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      message: 'Invalid request',
      code: 400,
      debug: `Cookie is invalid`,
    })
  }

  const getCurrentUser = await currentUser(cookie, authorizationModel)

  const { loggedInUserSession, loggedInUserType, loggedInUserPermissions } =
    getCurrentUser

  const userMailDomain =
    loggedInUserSession.identity.traits.email.match(/@(.+)/)[1]

  const isAllowedMailAddress = [
    ...AcceptedExternalMails,
    ...AcceptedInternalMails,
  ].some((mail) => {
    const acceptedDomains = mail.match(/@(.+)/)?.[1] ?? ''
    return acceptedDomains.toLowerCase() === userMailDomain
  })

  if (!isAllowedMailAddress) {
    logStack.info({ createdPermissions: [] })

    response.status = 204

    return
  }

  const tuples = [
    {
      namespace: authorizationModel.subdivisions.namespace,
      object: authorizationModel.subdivisions.objects.energie_dego,
      relation: authorizationModel.subdivisions.relations.issuer,
      subject_id: loggedInUserSession.identity.id,
    },
    {
      namespace: authorizationModel.subdivisions.namespace,
      object: authorizationModel.subdivisions.objects.energie_dego,
      relation: authorizationModel.subdivisions.relations.access,
      subject_id: loggedInUserSession.identity.id,
    },
    {
      namespace: authorizationModel.organizations.namespace,
      object: authorizationModel.organizations.objects.dego,
      relation: authorizationModel.organizations.relations.issuer,
      subject_id: loggedInUserSession.identity.id,
    },
  ]

  const grantPermissions = await Promise.all(
    tuples.map(async (tuple) => {
      const response = await fetch(
        `${process.env.KETO_WRITE_URL}/admin/relation-tuples`,
        {
          body: JSON.stringify(tuple),
          method: 'PUT',
        },
      )

      return await response.json()
    }),
  )

  logStack.info({
    createdPermissions: JSON.stringify(tuples),
  })

  response.status = 204
}
