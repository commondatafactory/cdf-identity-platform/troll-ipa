// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { createUser } from '../../../services/create-user'
import { createRecoveryCode } from '../../../services/create-recovery-code'
import { mailAdminAccountInvitation } from '../../../custom-mails/account-admin-invitation'
import { sendMail } from '../../../utils/send-mail'
import { mailLayoutHTML } from '../../../custom-mails/mail-layout'
import { toFormattedDate } from '../../../utils/to-formatted-date'

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  const newCreatedUser = await createUser({
    identityData: request.body,
  })

  logStack.info({ newUserCreatedId: newCreatedUser.id })

  const recoveryData = await createRecoveryCode({
    id: newCreatedUser.id,
  })

  const mailData = {
    receiver: {
      name: {
        first: newCreatedUser.traits.name.first,
        last: newCreatedUser.traits.name.last,
      },
      mail: newCreatedUser.traits.email,
    },
    sender: {
      name: { presented: 'De VNG' },
    },
    service: 'DOOK',
    expirationDate: toFormattedDate(recoveryData.expires_at),
    verificationCode: recoveryData.recovery_code,
    verificationUrl: `${recoveryData.recovery_link}&screen=verify`,
    loginUrl: process.env.ADMIN_LOGIN_URL ?? '',
  }

  const accountInvitation = mailAdminAccountInvitation(mailData)

  await sendMail({
    to: newCreatedUser.traits.email,
    subject: accountInvitation.subject,
    html: mailLayoutHTML({
      title: accountInvitation.subject,
      content: accountInvitation.html,
    }),
  })

  logStack.info({ mail: '✅' })

  response.status = 200
  response.body = newCreatedUser
}
