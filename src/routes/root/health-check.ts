// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

export default async (context: Context) => {
  context.status = 200
}
