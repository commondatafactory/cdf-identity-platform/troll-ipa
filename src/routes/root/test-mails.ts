// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { mailLayoutHTML } from '../../custom-mails/mail-layout'
import { mailEditedName } from '../../custom-mails/mail-edited-name'
import { mailPasswordRecovery } from '../../custom-mails/mail-password-recovery'
import { sendMail } from '../../utils/send-mail'
import { mailAdminAccountInvitation } from '../../custom-mails/account-admin-invitation'
import { mailAccountResearchInvitation } from '../../custom-mails/account-research-invitation'

export default async ({ request, response }: Context) => {
  if (process.env.NODE_ENV !== 'development') {
    response.status = 404
    return
  }

  const receiver = {
    name: { first: 'Jan', last: 'Jannsen' },
    previousName: { first: 'Jannn', last: 'Jannnsen' },
    mail: 'jan-jannsen@mail.nl',
  }
  const senderPresented = {
    mail: 'poet-doetinchem@mail.nl',
    name: { presented: 'De VNG' },
  }
  const sender = {
    name: { first: 'Poet', last: 'Doetinchem' },
    mail: 'poet-doetinchem@mail.nl',
  }
  const loginUrl = '/login'
  const service = 'DOOK'
  const organization = 'Doetichem'
  const verificationCode = '123456'
  const verificationUrl = 'https://google.nl'

  const date = new Date()
  date.setDate(date.getDate() + 7)

  const divisionsToActivate = Array.from(request.query.n ?? []).map((n) =>
    Number(n),
  )

  // account maken ADMIN
  if (divisionsToActivate.includes(1)) {
    const accountInvitation = mailAdminAccountInvitation({
      expirationDate: date.toLocaleDateString('nl-NL', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
      }),
      receiver,
      sender: senderPresented,
      service,
      organization,
      verificationCode,
      verificationUrl,
      loginUrl,
    })

    console.log('\x1b[35m', accountInvitation.subject, '\x1b[0m')

    await sendMail({
      to: receiver.mail,
      subject: accountInvitation.subject,
      html: mailLayoutHTML({
        title: accountInvitation.subject,
        content: accountInvitation.html,
      }),
    })
  }

  // account maken RESEARCHER
  if (divisionsToActivate.includes(1)) {
    const accountInvitation = mailAccountResearchInvitation({
      receiver,
      loginUrl,
    })

    console.log('\x1b[35m', accountInvitation.subject, '\x1b[0m')

    await sendMail({
      to: receiver.mail,
      subject: accountInvitation.subject,
      html: mailLayoutHTML({
        title: accountInvitation.subject,
        content: accountInvitation.html,
      }),
    })
  }

  // bevestiging naam aangepast
  if (divisionsToActivate.includes(2)) {
    const nameEdit = mailEditedName({
      receiver,
      sender,
      loginUrl,
    })

    console.log('\x1b[35m', nameEdit.subject, '\x1b[0m')

    await sendMail({
      to: receiver.mail,
      subject: nameEdit.subject,
      html: mailLayoutHTML({
        title: nameEdit.subject,
        content: nameEdit.html,
      }),
    })
  }

  // wachtwoord hersteld
  if (divisionsToActivate.includes(3)) {
    const passwordRecovery = mailPasswordRecovery({
      receiver,
      loginUrl,
      expirationDate: date.toLocaleDateString('nl-NL', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
      }),
      verificationCode,
      verificationUrl,
    })

    console.log('\x1b[35m', passwordRecovery.subject, '\x1b[0m')

    await sendMail({
      to: receiver.mail,
      subject: passwordRecovery.subject,
      html: mailLayoutHTML({
        title: passwordRecovery.subject,
        content: passwordRecovery.html,
      }),
    })
  }

  response.status = 204
}
