// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  organizationModels,
  availableRoleModels,
} from '../utils/permission-models'
import { currentUser } from '../services/get-current-session'
import { isString } from '../utils/validators'
import { CustomError } from '../errors/response-error'
import { type Context } from 'koa'

export default async ({ request, response, authorizationModel }: Context) => {
  const cookie = request.headers.cookie

  const isValidRequest = isString(cookie)

  if (!isValidRequest) {
    throw new CustomError({
      message: 'Invalid request',
      code: 400,
      debug: `cookie is invalid`,
    })
  }

  const getCurrentUser = await currentUser(cookie, authorizationModel)

  const { loggedInUserPermissions } = getCurrentUser

  // TODO: differentiate between access, issuer and admin
  const userOrganizationModels = organizationModels(authorizationModel).filter(
    (model) =>
      loggedInUserPermissions.some(
        (permission) => model.id === permission.object,
      ),
  )

  const userAvailableRoleModels = availableRoleModels(
    authorizationModel,
  ).filter((model) =>
    loggedInUserPermissions.some(
      (permission) => permission.object === model.id,
    ),
  )

  const data = [...userOrganizationModels, ...userAvailableRoleModels]

  response.status = 200
  response.body = data
}
