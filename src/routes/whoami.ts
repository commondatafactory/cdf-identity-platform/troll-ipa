// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { type Context } from 'koa'

import { getKratosWhoami } from '../services/get-kratos-whoami'
import { getPermissions } from '../services/get-permissions'
import { getUserType } from '../utils/get-user-type'
import {
  type UserType,
  userTypeLabels,
  organizationModels,
  availableRoleModels,
} from '../utils/permission-models'

export default async ({
  request,
  response,
  authorizationModel,
  logStack,
}: Context) => {
  const loggedInUserSession = await getKratosWhoami(request.headers.cookie)

  logStack.info({ user_identity: loggedInUserSession.identity.id })

  const query = new URLSearchParams({
    subject_id: loggedInUserSession.identity.id,
  }).toString()

  const loggedInUserPermissions = await getPermissions(query)

  const userPermissionWithLabels = loggedInUserPermissions.map(
    (permission) => ({
      ...permission,
      ...organizationModels(authorizationModel).find(
        (model) => permission.object === model.id,
      ),
      ...availableRoleModels(authorizationModel).find(
        (model) => permission.object === model.id,
      ),
    }),
  )

  const loggedInUserType: UserType = getUserType(
    loggedInUserPermissions,
    authorizationModel,
  )

  const userTypeLabel = userTypeLabels.find(
    (labels) => labels.id === loggedInUserType,
  )

  const sortedPermissions = userPermissionWithLabels.reduce(
    (acc, permission) => {
      const { namespace, relation } = permission

      if (!acc[namespace]) {
        acc[namespace] = {}
      }

      acc[namespace] = {
        ...acc[namespace],
        [relation]: [...(acc[namespace][relation] || []), permission],
      }

      return acc
    },
    {},
  )

  response.status = 200
  response.body = {
    ...loggedInUserSession,
    permissions: userPermissionWithLabels,
    sortedPermissions,
    userType: userTypeLabel,
  }
}
