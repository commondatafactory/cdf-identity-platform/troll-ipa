// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { stripHtml } from '../utils/strip-html'

export const mailPasswordRecovery = ({
  receiver,
  loginUrl,
  expirationDate,
  verificationCode,
  verificationUrl,
}: {
  loginUrl: string
  receiver: {
    name: { first: string; last: string }
    mail: string
  }
  expirationDate: string
  verificationCode: string
  verificationUrl: string
}) => ({
  subject: `Stel je nieuwe wachtwoord in`,
  html: `
  Beste ${receiver.name.first} ${receiver.name.last},<br/><br/>

  Je hebt aangegeven je wachtwoord te zijn vergeten. Geen probleem: dat overkomt de beste!
  
  Maak een nieuw wachtwoord aan via deze <a href=${verificationUrl}>link</a>. <br /><br />
    
  Gebruik daarvoor deze gegevens: <br />
  
  <h3>Verificatiecode: ${verificationCode}</h3>
  Let op! Deze code is slechts eenmalig geldig en vervalt op ${expirationDate}. <br />
  `,
  text() {
    stripHtml(this.html)
  },
})
