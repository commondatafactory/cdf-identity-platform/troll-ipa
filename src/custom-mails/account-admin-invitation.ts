// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { stripHtml } from '../utils/strip-html'

export const mailAdminAccountInvitation = ({
  expirationDate,
  loginUrl,
  receiver,
  sender,
  service,
  verificationCode,
  verificationUrl,
}: {
  expirationDate: string
  loginUrl: string
  receiver: { name: { first: string; last: string }; mail: string }
  sender: { name: { presented: string } }
  service: string
  verificationCode: string
  verificationUrl: string
}) => ({
  subject: `We hebben een account voor je aangemaakt`,
  html: `
    Beste ${receiver.name.first} ${receiver.name.last},<br /><br />

    ${sender.name.presented} heeft een account voor je aangemaakt voor de datavoorziening ${service}. <br /><br />

    Je kan eenmalig een wachtwoord aanmaken via deze <a href=${verificationUrl}>link</a>. <br /><br />
    
    Gebruik daarvoor deze gegevens: <br />
    
    <h3>Verificatiecode: ${verificationCode}</h3>
    Let op! Deze code is slechts eenmalig geldig en vervalt op ${expirationDate}. <br /><br />
    
    Bekijk je account en welke data-permissies je hebt <a href=${loginUrl}>hier</a>.<br />
    
    Gebruik daarvoor deze gegevens:
    <ul>
      <li>E-mailadres: <strong>${receiver.mail}</strong></li>
      <li>Wachtwoord: het wachtwoord wat aangemaakt hebt bij het verifiëren van het account.</li>
    </ul>
    `,
  text() {
    stripHtml(this.html)
  },
})
