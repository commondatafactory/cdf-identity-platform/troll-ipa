// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { stripHtml } from '../utils/strip-html'

export const mailEditedName = ({
  loginUrl,
  receiver,
  sender,
}: {
  loginUrl: string
  receiver: {
    name: { first: string; last: string }
    previousName: { first: string; last: string }
    mail: string
  }
  sender: {
    name: { first: string; last: string }
    mail: string
  }
}) => ({
  subject: `We hebben de gebruikersnaam aangepast van je account`,
  html: ` 
  Beste ${receiver.name.first} ${receiver.name.last},<br /><br />

  Je accountgegevens zijn gewijzigd voor de datavoorziening. De gebruikersnaam is gewijzigd van <strong>${receiver.previousName.first} ${receiver.previousName.last}</strong> naar <strong>${receiver.name.first} ${receiver.name.last}</strong>.<br /><br />

  Bekijk je account op: <a href=${loginUrl}>${loginUrl}</a>.<br /><br />
  
  Was dit niet de bedoeling, neem dan contact met de beheerder: ${sender.name.first} ${sender.name.last}. <a href="mailto:${sender.mail}">${sender.mail}</a>.<br />
`,
  text() {
    stripHtml(this.html)
  },
})
