// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import mjml2html from 'mjml'
import { mailDefaultFooter } from './mail-footer-default'

export const mailLayoutHTML = ({ title, content }) => {
  const mail = mjml2html(
    `<mjml version="4.9.3">
    <mj-head>
      <mj-title>titel</mj-title>
      <mj-attributes>
        <mj-all font-family="Arial" />
        <mj-text padding="0px" font-size="16px" line-height="150%" />
        <mj-section padding="16px 0 0 0" />
        <mj-class name="blue" color="#2f5496" />
        <!-- mj-class werkt alleen op mj-elementen-->
      </mj-attributes>
      <mj-style>
        ul {margin-bottom: 0;}
        .highlight {color: #2f5496; font-weight: 700;}
        .font-small {font-size: 14px; line-height: 150%;}
      </mj-style>
    </mj-head>
  
    <mj-body background-color="#F6F6F6">
      <mj-spacer height="32px" />
      <mj-section
        border-radius="8px 8px 0 0"
        background-color="#FFFFFF"
        background-repeat="repeat"
        text-align="center"
      >
        <mj-column padding="32px">
          <mj-text>
            ${content}
            ${mailDefaultFooter().html}
          </mj-text>
        </mj-column>
      </mj-section>
    </mj-body>
    </mjml>`,
    { juicePreserveTags: true },
  )

  return mail.html
}
