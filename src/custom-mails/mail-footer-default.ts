// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { stripHtml } from '../utils/strip-html'

export const mailDefaultFooter = () => ({
  html: `<br />
  <div class="font-small">
    ─ <br /><br />
    <span>Met vriendelijke groet,</span>
    <br /><br />
    <strong>Namens het team Versterken Informatie Positie (VIP)</strong>
    <br />
    <span>Data & Monitoring</span>
    <br /><br />
    <span class="highlight">Vereniging van Nederlandse Gemeenten</span>

    <br /><br />

    <a href="dego.vng.nl">dego.vng.nl</a> - Team VIP - <a href="mailto:vip@vng.nl">vip@vng.nl</a>
    <br />
    <a href="dook.vng.nl">dook.vng.nl</a> - Abel Koppert - <a href="tel:0681173527">06 81173527<a />
    <br />
    <a href="vng.nl">vng.nl<a />
  </div>
    `,
  text() {
    stripHtml(this.html)
  },
})
