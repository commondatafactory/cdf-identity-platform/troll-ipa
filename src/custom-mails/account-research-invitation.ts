// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { stripHtml } from '../utils/strip-html'

export const mailAccountResearchInvitation = ({
  expirationDate,
  loginUrl,
  receiver,
  verificationCode,
  verificationUrl,
}: {
  expirationDate: string
  loginUrl: string
  receiver: {
    name: {
      first: string
      last: string
    }
    mail: string
  }
  verificationCode: string
  verificationUrl: string
}) => ({
  subject: `We hebben een account voor je aangemaakt`,
  html: `
    Beste ${receiver.name.first} ${receiver.name.last}, <br /><br />
    
    Welkom bij de VNG data voorzieningen. Zojuist is een account gemaakt op dit e-mailadres. <br /><br />

    Je kan eenmalig je account verifiëren via deze <a href=${verificationUrl}>link</a>. <br /><br />
    
    Gebruik daarvoor deze gegevens: <br />
    
    <h3>Verificatiecode: ${verificationCode}</h3>
    Let op! Deze code is slechts eenmalig geldig en vervalt op ${expirationDate}. <br /><br />

    Bekijk je account en welke data-permissies je hebt <a href=${loginUrl}>hier</a>.<br />
    `,
  text() {
    stripHtml(this.html)
  },
})
