// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { type Context, type Next } from 'koa'

export const getTime = async (ctx: Context, next: Next): Promise<Next> => {
  const start = Date.now()
  const ms = Date.now() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms} ms`)
  return await next()
}
