// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { type Next, type Context } from 'koa'
import { CustomError } from 'src/errors/response-error'

export const errorHandler = async (context: Context, next: Next) => {
  try {
    await next()
  } catch (error) {
    const httpStatusCode = error.code || 500
    let message =
      error.message ||
      'Er heeft zich een interne serverfout voorgedaan, neem contact op met de systeembeheerder.'

    // NOTE: if it's node related
    if (!(error instanceof CustomError)) {
      console.log('LOG ERROR: !(error instanceof CustomError')

      if (process.env.NODE_ENV !== 'production') {
        if (typeof error === 'string') {
          message = error
        } else if (error instanceof Error) {
          message = error.message
        }
      }

      context.logStack.error({
        error: {
          status: httpStatusCode,
          message,
          stackTrace: error.stack,
        },
      })

      context.response.status = httpStatusCode
      context.response.message = message

      return
    }

    const filteredError = {
      message,
      ...(error.code ? { code: error.code } : {}),
      ...(error.debug ? { debug: error.debug } : {}),
      ...(error.details ? { details: error.details } : {}),
      ...(error.id ? { id: error.id } : {}),
      ...(error.reason ? { reason: error.reason } : {}),
      ...(error.request ? { request: error.request } : {}),
      ...(error.status ? { status: error.status } : {}),
    }

    context.logStack.error(filteredError)

    if (httpStatusCode === 404) {
      context.response.status = httpStatusCode
      return
    }

    context.response.status = httpStatusCode
    context.response.body = {
      error: filteredError,
    }
  }
}
