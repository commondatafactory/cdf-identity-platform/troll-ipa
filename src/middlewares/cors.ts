// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import corsKoa from '@koa/cors'
import { type Middleware, type Context, type Next } from 'koa'
import { CustomError } from 'src/errors/response-error'

export const cors = (): Middleware => {
  return async (ctx: Context, next: Next) => {
    const corsAllowedOrigins = {
      kubernetesHosted: [
        '.example.com',
        '.vng.com',
      ],
      development: ['.example.com', '.vng.com'],
      production: ['.commondatafactory.nl', '.vng.nl'],
    }

    const currentEnv = process.env.DEPLOY_ENV ?? process.env.NODE_ENV

    if (!currentEnv) {
      throw new CustomError({
        code: 500,
        message: 'Invalid request',
        debug: `CORS environment variable not found`,
      })
    }

    const allowedOrigins: string[] = corsAllowedOrigins[currentEnv]

    const corsOrigin = allowedOrigins.some((origin) =>
      ctx.headers.origin?.toLowerCase().includes(origin),
    )
      ? ctx.headers.origin
      : ''

    //console.log("CORS: ", corsOrigin, ctx.headers.origin, ctx.headers)

    await corsKoa({
      allowMethods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
      origin: corsOrigin,
      credentials: true,
      keepHeadersOnError: true,
    })(ctx, next)
  }
}
