// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { type Context, type Next } from 'koa'
import { LogStack } from 'src/utils/log-stack'

export const logging =
  (serverType: 'public' | 'private') => async (ctx: Context, next: Next) => {
    ctx.logStack = new LogStack()

    ctx.logStack.setLoggingServer(serverType)

    ctx.logStack.info({ method: ctx.request.method, url: ctx.request.url })

    await next()

    ctx.logStack.info({ status: ctx.response.status })
    ctx.logStack.log()
  }
