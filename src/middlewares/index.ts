// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { cors } from './cors'
import { errorHandler } from './error-handler'
import { logging } from './logging'

export default { cors, errorHandler, logging }
