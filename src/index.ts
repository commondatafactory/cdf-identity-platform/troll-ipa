// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import cron from 'node-cron'
import dotenv from 'dotenv'
import Koa from 'koa'
import koaBody from 'koa-body'
import Router from '@koa/router'

import router from '@routes/index'
import middlewares from '@middlewares/index'
import createAccount from '@routes/root/create-account'
import testMails from '@routes/root/test-mails'
import { getContextAuthModel } from 'src/utils/get-auth-model'

dotenv.config({ path: './.env.development' })

const HOSTNAME = 'localhost'
const PORT_PUBLIC = 3003
const PORT_PRIVATE = 3004

const startPublicServer = async (): Promise<Koa> => {
  const app = new Koa()

  app.use(middlewares.logging('public'))

  app.use(middlewares.errorHandler)

  app.use(koaBody())

  app.use(middlewares.cors())

  app.use(router.root.routes())
  app.use(router.permissions.routes())
  app.use(router.users.routes())

  return app
}

const startPrivateServer = async (): Promise<Koa> => {
  const app = new Koa()

  app.use(middlewares.logging('private'))

  app.use(middlewares.errorHandler)

  app.use(koaBody())

  const root = new Router()
  root.get('/', (context) => {
    context.message = 'Private port - Ok'
  })
  root.post('/create-account', createAccount)
  root.get('/test-mails', testMails)
  app.use(root.routes())

  return app
}

startPrivateServer()
  .then(async (app) => {
    console.log(
      '\x1b[40m',
      '\x1b[35m',
      `▶ Ready on http://${HOSTNAME}:${PORT_PRIVATE}`,
      '\x1b[0m',
    )

    app.listen(PORT_PRIVATE)
  })
  .catch(console.error)

startPublicServer()
  .then(async (app) => {
    console.log('\x1b[40m', '\x1b[31m', `Environment variables:`, '\x1b[0m')
    console.log('\x1b[2m')

    console.log(`DEPLOY_ENV:          ${process.env.DEPLOY_ENV ?? null}`)
    console.log(`NODE_ENV:            ${process.env.NODE_ENV}`)
    console.log(`KRATOS_ADMIN_URL:    ${process.env.KRATOS_ADMIN_URL}`)
    console.log(`KRATOS_PUBLIC_URL:   ${process.env.KRATOS_PUBLIC_URL}`)
    console.log(`AUTH_MODELS_URL:     ${process.env.AUTH_MODELS_URL}`)
    console.log(`ANALYST_LOGIN_URL:     ${process.env.ANALYST_LOGIN_URL}`)

    console.log('\x1b[0m')

    if (
      typeof process.env.NODE_ENV !== 'string' ||
      typeof process.env.KRATOS_ADMIN_URL !== 'string' ||
      typeof process.env.KRATOS_PUBLIC_URL !== 'string' ||
      typeof process.env.AUTH_MODELS_URL !== 'string' ||
      typeof process.env.ANALYST_LOGIN_URL !== 'string'
    ) {
      console.error('Missing environment variables. Exiting server.')
      process.exit(1)
    }

    console.log(
      '\x1b[40m',
      '\x1b[35m',
      `▶ Ready on http://${HOSTNAME}:${PORT_PUBLIC}`,
      '\x1b[0m',
    )

    const authModel = await getContextAuthModel()
    app.context.authorizationModel = authModel

    let listener = app.listen(PORT_PUBLIC)

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    cron.schedule('*/20 8-20 * * 1-5', async () => {
      const authModel = await getContextAuthModel()
      app.context.authorizationModel = authModel

      console.log('Resetting listener')

      listener.close(() => {
        console.log('Starting new listener')
        listener = app.listen(PORT_PUBLIC)
      })
    })
  })
  .catch(console.error)
