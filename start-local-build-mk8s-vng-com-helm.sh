#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error

NAMESPACE=vng
DEPLOY_ENV=kubernetesHosted

docker build --tag localhost:32000/auth-service-api:1.0.0 .
docker push localhost:32000/auth-service-api:1.0.0

helm delete -n vng auth-service-api || true

helm upgrade --install -n $NAMESPACE -f ./helm/values.vng.com.mk8s.yaml auth-service-api ./helm

kubectl delete -n $NAMESPACE -f ./helm/ingress.vng.com.yaml || true
kubectl apply -n $NAMESPACE -f ./helm/ingress.vng.com.yaml
