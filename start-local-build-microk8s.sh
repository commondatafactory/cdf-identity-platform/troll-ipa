#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error

docker build --tag localhost:32000/auth-service-api:1.0.0 .

# make sure we are on the right cluster
kubectl config use-context microk8s-laptop

helm delete -n ory auth-service-api || true

helm upgrade --install -n ory -f ./helm/values.mk8s.yaml auth-service-api ./helm


kubectl apply -n ory -f ./helm/ingress.example.yaml

