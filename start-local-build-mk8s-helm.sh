#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error

NAMESPACE=ory
DEPLOY_ENV=kubernetesHosted

docker build --tag localhost:32000/auth-service-api:1.0.0 .
docker push localhost:32000/auth-service-api:1.0.0

helm delete -n ory auth-service-api || true

helm upgrade --install -n $NAMESPACE -f ./helm/values.mk8s.yaml auth-service-api ./helm

kubectl delete -n $NAMESPACE -f ./helm/ingress.example.yaml
kubectl apply -n $NAMESPACE -f ./helm/ingress.example.yaml
