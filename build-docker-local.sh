#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error

docker build --tag auth-service-api:1.0.0 .

GREEN=$'\e[0;32m'
RED=$'\e[0;31m'
NC=$'\e[0m'
echo "✓ ${RED}STARTING${NC} ${GREEN}DOCKER CONTAINER${NC} ON PORT 3003"

docker run \
    --rm \
    --name auth-service-api \
    -p 3003:3003 \
    -e NODE_ENV=production \
    -e KRATOS_PUBLIC_URL=http://kratos-public \
    -e KRATOS_ADMIN_URL=http://kratos-admin \
    -e AUTH_MODELS_URL=https://gitlab.com/commondatafactory/rbac-models/-/raw/main/authorization-model-dev.json \
    auth-service-api:1.0.0
