FROM node:alpine

WORKDIR /app

ENV NODE_ENV production
ENV DEPLOY_ENV kubernetesHosted

COPY package.json .
COPY yarn.lock .

RUN yarn --production && yarn cache clean

COPY . .

RUN yarn build

EXPOSE 3003

CMD ["yarn", "start"]
