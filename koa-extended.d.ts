import Koa from 'koa'
import type AuthorizationModel from './src/authorization-model.json'
import { LogStack } from 'src/utils/log-stack'

declare module 'koa' {
  interface Context {
    authorizationModel: typeof AuthorizationModel
    logStack: LogStack
  }
  interface ExtendableContext {
    authorizationModel: typeof AuthorizationModel
    logStack: LogStack
  }
}

export default Koa
