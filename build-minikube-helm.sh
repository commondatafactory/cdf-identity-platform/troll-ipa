#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error

minikube image build -t auth-service-api:1.0.0 .
# eval $(minikube docker-env)
# docker build --tag auth-service-api:1.0.0 .

helm delete -n ory auth-service-api || true
helm upgrade --install -n ory -f ./helm/values.minikube.yaml auth-service-api ./helm

kubectl apply -n ory -f ./helm/ingress.dev.yaml
