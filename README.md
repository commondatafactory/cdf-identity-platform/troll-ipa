# Autorisatie API

Welkom bij de README van de autorisatie API.

Hierin staat het volgende beschreven:

- Wat biedt de API
- Hoe werkt de API
- Hoe draai je de API

## Wat biedt de API

Deze API is specifiek gemaakt voor [DEGO/DOOK](https://commondatafactory.nl). Het biedt gebruikers van de datavoorzieningen de mogelijkheid om specifieke permissies tot data te verkrijgen (lees: activeren), indien de gebruikers ook rechtmatig toegang tot de data hebben.

Deze autorisatie API fungeert als extensie van de [Ory Kratos](https://www.ory.sh/kratos) en [Ory Keto](https://www.ory.sh/keto) API's. Deze API geeft toegang tot functies die afgeschermd zijn voor gebruikers van de datavoorzieningen.

### Welke extensies biedt de API in Ory Kratos en Ory Keto

Met behulp van deze autorisatie API kun je de volgende wijzigingen maken, mits je de juiste toegang hebt:

- Gegevens van andere gebruikers inzien
- Gegevens van andere gebruikers wijzigen
- Permissies inzien voor eigen account
- Permissies inzien voor voor andere gebruikers
- Permissies wijzigen bij eigen account
- Permissies wijzigen voor andere gebruikers
- Permissies wijzigen voor andere gebruikers
- Bij wijzigingen voor andere gebruikers hen op de hoogte stellen via e-mail




## Hoe werkt de API

De autorisatie API dient als proxy voor gebruikers hoofdzakelijk via een front-end en de API's van Ory Kratos en Ory Keto. Gebruikers kunnen verzoeken (requests) indienen en mits zij de juiste permissies hebben worden bovenstaande verzoeken afgehandeld.

### Configuratie

Gebruikers en hun mogelijke permissies worden gebaseerd op één enkel [configuratiebestand](https://gitlab.com/commondatafactory/rbac-models). Dit bestand wordt periodiek door de API opgehaald om snel veranderingen door te kunnen voeren.

### Configuratie: Categorieën

In de kern van de API wordt onderscheid gemaakt organisaties, subdivisies en databronnen.

- Organisaties dienen als ankerpunt voor gebruikers waar alle permissies onder behoren te vallen. Databronnen zijn gekoppeld aan organisaties.

- Subdivisies zijn verzamelingen aan databronnen. Dit is als een mand waar verschillende databronnen in kunnen.

- Databronnen zijn directe verwijzingen naar data en verschaft de uiteindelijke permissie voor een desbetreffende databron.


### Configuratie: gebruikers

In de API wordt onderscheid gemaakt tussen administratoren en onderzoekers.

- Administratoren behoren bij organisaties en managen de gebruikers binnen organisaties. Deze kunnen onderzoekers indien en wijzigen binnen hun organisaties. Daarnaast kunnen zij ook alles wat een onderzoeker kan.

- Onderzoekers hebben inzage in hun eigen permissies en kunnen deze activeren en deactiveren.

### Configuratie: private en public porten

In de API wordt onderscheid gemaakt tussen twee porten. Een `public` en een `private` port. De publieke port is openbaar beschikbaar en handelt verzoeken af gericht op CRUD acties rondom gebruikers. De private port is ontworpen om alleen beschikbaar te zijn binnen de omgeving van het Kubernetes Cluster. Via deze port en bijbehorende endpoint is het mogelijk om een nieuwe gebruiker te maken in Ory Kratos en tevens gebruik te maken van de e-mail functie die de API biedt.

We beheren een Python script ontworpen om dit endpoint aan te spreken te vinden op onze [Ory repository](https://gitlab.com/commondatafactory/cdf-identity-platform/ory/-/blob/main/dev-scripts/create_user_and_mail.py?ref_type=heads).


## Hoe draai je de API

De minimale vereisten om een werkende versie van de autorisatie API te hebben wordt hier beschreven.

### Vereisten

1. [Node.js](https://nodejs.org/en)
1. [Yarn](https://yarnpkg.com)
1. [Een draaiende versie van Ory Kratos](https://www.ory.sh/kratos)
1. [Een draaiende versie van Ory Keto](https://www.ory.sh/keto)
1. [Configuratie voor Ory](https://gitlab.com/commondatafactory/cdf-identity-platform/ory)

### Via localhost

Je kan de API direct op je OS draaien via de volgende stappen:

1. In het `.env.development` bestand staan de porten beschreven waarmee de autorisatie API connectie maakt met Kratos en Keto. Deze dienen te draaien op de genoemde porten. Wij raden aan om Kratos en Keto te draaien op een lokaal [Minikube](https://minikube.sigs.k8s.io) cluster.
1. Draai de API zoals beschreven staat in het `dev` commando van de `package.json`

### Via een lokaal cluster

Je kan de API draaien op een lokaal cluster via de volgende stappen:

1. Gebruik `.env.development.cluster`
1. in je `etc hosts` link `login.example.com` naar je 127.0.0.1
1. Laad de API via `http://login.example.com:3003`
1. Zorg dat al je lokaal toegang hebt tot je k8s ingresses
