declare global {
  namespace NodeJS {
    interface ProcessEnv {
      KRATOS_ADMIN_URL: string;
      KRATOS_PUBLIC_URL: string;
      KETO_READ_URL: string;
      AUTH_MODELS_URL: string;
      ANALYST_LOGIN_URL: string
    }
  }
}

// Export an empty object to make it a module
export {};
